# Generador Morse Electrónico

En el mundo de la radioafición la transmisión de Código Morse en el modo CW 
(Continous Wave) es muy usual.


Para facilitar el entrenar en la generación de código Morse se propone un
circuito con las siguientes características:

* Control de volumen
* Control de tono
* Transmisión por luz y por audio


Se propone además la construcción de una llave muy sencilla y cómoda a la vez.


En la carpeta *[morse](morse)* se encuentra el esquemático y el PCB listo para
su creación.


